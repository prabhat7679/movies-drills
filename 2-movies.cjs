const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}

/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 
  // Q1. Find all the movies with total earnings more than $500M. 

  function totalEarning(){
    let totalEarningMoreThan =Object.entries(favouritesMovies);

   // console.log(totalEarningMoreThan)
    const totalEarningMoreThan500 = totalEarningMoreThan.filter((data)=>{
       //  console.log(data[1].totalEarnings);
         const earning = Number(data[1].totalEarnings.slice(1).replace('M',''));
         
         return earning > 500;
    })
    console.log(totalEarningMoreThan500)
  }
  totalEarning()
 

//   let totalEarningMoreThan = Object.keys(favouritesMovies).reduce((acc, myMovie)=>{
//   //  console.log(myMovie);
//     let earning = favouritesMovies[myMovie].totalEarnings;
    
//     earning =Number(earning.slice(1,4));
//    // console.log(earning);
//     if(earning > 500)
//     {
//      acc[myMovie] =favouritesMovies[myMovie];
//     }
//      return acc;
//   },{}) 

//  console.log(totalEarningMoreThan)


  let totalEarningMoreThanAndOscar = Object.keys(favouritesMovies).reduce((acc, myMovie)=>{
    //  console.log(myMovie);
      let earning = favouritesMovies[myMovie].totalEarnings;
      let oscar = favouritesMovies[myMovie].oscarNominations;
      
      earning =Number(earning.slice(1,4));
     // console.log(earning);
      if(earning > 500 && oscar > 3){
         acc[myMovie] =favouritesMovies[myMovie];
       }
       return acc;
    },{}) 

 //   console.log(totalEarningMoreThanAndOscar)  

   //     Q.3 Find all movies of the actor "Leonardo Dicaprio".
   let acctorsLeonardo = Object.keys(favouritesMovies).reduce((acc, myMovie)=>{

     if(favouritesMovies[myMovie].actors.includes('Leonardo Dicaprio')){
       acc[myMovie] =favouritesMovies[myMovie];
      }
      return acc;
    },{}) 
  //  console.log(acctorsLeonardo)

//  Q.4 Sort movies (based on IMDB rating)
//  if IMDB ratings are same, compare totalEarning as the secondary metric.


 
let sorted = Object.entries(favouritesMovies).sort((movie1,movie2)=>{

  // console.log(movie1[1].imdbRating )

   if(movie1[1].imdbRating > movie2[1].imdbRating){
    return -1;
   }else if(movie1[1].imdbRating < movie2[1].imdbRating){
      return 1;
   }else{
     let totalEarn1 = Number(movie1[1].totalEarnings.slice(1).replace('M',''));
     let totalEarn2= Number(movie2[1].totalEarnings.slice(1).replace('M',''));

     if(totalEarn1 > totalEarn2){
        return -1;
     }else{
        return 1;
     }
   }
}) 
    console.log(sorted)

//   Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//   drama > sci-fi > adventure > thriller > crime


const priority = ["drama", "sci-fi", "adventure", "thriller", "crime"];

const groupedMovies = {};

const answer =  priority.map(genre => {
  groupedMovies[genre] = Object.keys(favouritesMovies)
    .filter((movie) =>{
        return favouritesMovies[movie].genre.includes(genre)
        });
        return groupedMovies[genre];
});

console.log(groupedMovies);

